#log to journal stating that its working 
systemd-cat -p "notice" -t wp_mariadb_config printf "%s" "wp_mariadb_config.sh start" 

mysql -u root < /root/wp_mariadb_config/wp_mariadb_config.sql
mysql -u root < /root/wp_mariadb_config/mariadb_security_config.sql

systemctl disable wp_mariadb_config.service

systemd-cat -p "notice" -t wp_mariadb_config printf "%s" "wp_mariadb_config.sh end" 

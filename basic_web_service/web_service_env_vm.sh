#!/usr/bin/env bash

#vm settings
vm_name='test'
#$vms_folder
network_name=nasp_cm_co
iso_path='CentOS-7-x86_64-Minimal-1804.iso'

vboxmanage createvm --name $vm_name --register

vboxmanage storagectl $vm_name --name controller --add sata --bootable on --portcount 3

vboxmanage createmedium disk --filename ./$vm_name.vdi --size 8000
vboxmanage storageattach $vm_name --port 1 --storagectl controller --type hdd --medium ./$vm_name.vdi
vboxmanage storageattach $vm_name --port 2 --storagectl controller --type  dvddrive --medium ./$iso_path

vboxmanage storageattach $vm_name --storagectl controller --port 3 --type dvddrive --medium "/usr/share/virtualbox/VBoxGuestAdditions.iso"

vboxmanage modifyvm $vm_name\
    --ostype "RedHat_64"\
    --cpus 1\
    --hwvirtex on\
    --nestedpaging on\
    --largepages on\
    --firmware bios\
    --nic1 natnetwork\
    --nictype1 "82543gC"\
    --nat-network1 "$network_name"\
    --cableconnected1 on\
    --audio none\
    --boot1 disk\
    --boot2 dvd\
    --boot3 none\
    --boot4 none\
    --memory "2000"

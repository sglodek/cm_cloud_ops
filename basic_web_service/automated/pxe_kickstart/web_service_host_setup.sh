#!/usr/bin/env bash
yum -y groupinstall base
yum -y update
yum -y install epel-release
yum -y install vim git tcpdump nmap-ncat nginx mariadb mariadb-server php php-mysql php-fpm wget

adduser admin -G wheel -p nasp20
systemctl disable firewalld
systemctl stop firewalld

#-----------------------------------------
#nginx
#-----------------------------------------
systemctl enable nginx
echo """
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    log_format  main  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "\$http_referer" '
                      '"\$http_user_agent" "\$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
	index index.php index.html index.htm;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
	location ~ \.php$ {
	    try_files \$uri =404;
	    fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
            fastcgi_index index.php;
	    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
	    include fastcgi_params;
        }
    }
}
""" > /etc/nginx/nginx.conf
systemctl start nginx

#-----------------------------------------
#mariaDB
#-----------------------------------------
systemctl enable mariadb
systemctl start mariadb
echo """
# Set root password
UPDATE mysql.user SET Password=PASSWORD('nasp20') WHERE User='root';

# Remove anonymous users
DELETE FROM mysql.user WHERE User='';

# Disallow remote root login
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');

# Remove test database
DROP DATABASE test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
""" > mysqlsecurity
mysql -u root < mysqlsecurity

echo """
## Wordpress Database Setup
CREATE DATABASE wordpress;
CREATE USER wordpress_user@localhost IDENTIFIED BY 'nasp20';
GRANT ALL PRIVILEGES ON wordpress.* TO wordpress_user@localhost;

# Reload privilege tables
FLUSH PRIVILEGES;
""" > mysqlconfig
mysql -u root < mysqlconfig

mysql -u root --password=nasp20 -e "SELECT user FROM mysql.user;"
mysql -u root --password=nasp20 -e "SHOW DATABASES;"


#-----------------------------------------
#php
#-----------------------------------------
systemctl enable php-fpm
#sed /etc/php.ini php line 763 cgi.fix_pathinfo=0
sed '763 s/.*/cgi.fix_pathinfo=0/' /etc/php.ini > /etc/php2.ini
mv -f /etc/php2.ini /etc/php.ini

#sed /etc/php-fpm.d/www.conf line 12 listen = /var/run/php-fpm/php-fpm.sock
#sed /etc/php-fpm.d/www.conf line 31 listen.owner = nobody
#sed /etc/php-fpm.d/www.conf line 32 listen.group = nobody
#sed /etc/php-fpm.d/www.conf line 39 user = nginx
#sed /etc/php-fpm.d/www.conf line 41 group = nginx

sed -e '12 s/.*/listen = \/var\/run\/php-fpm\/php-fpm.sock/' -e '31 s/.*/listen.owner = nobody/' -e '32 s/.*/listen.group = nobody/' -e '39 s/.*/user = nginx/' -e '41 s/.*/group = nginx/' /etc/php-fpm.d/www.conf > /etc/php-fpm.d/www2.conf
mv -f /etc/php-fpm.d/www2.conf /etc/php-fpm.d/www.conf
systemctl start php-fpm











#-----------------------------------------
#wordpress
#-----------------------------------------
wget http://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
cp wordpress/wp-config-sample.php wordpress/wp-config.php
#sed wordpress/wp-config.php line 23 define('DB_NAME', 'wordpress');
#sed wordpress/wp-config.php line 26 define('DB_USER', 'wordpress_user');
#sed wordpress/wp-config.php line 29 define('DB_PASSWORD', 'nasp20');
sed -e '23 s/.*/define('DB_NAME', 'wordpress');/' -e '26 s/.*/define('DB_USER', 'wordpress_user');/' -e '29 s/.*/define('DB_PASSWORD', 'nasp20');/' wordpress/wp-config.php > wordpress/wp-config2.php
mv -f wordpress/wp-config2.php wordpress/wp-config.php

rsync -avP wordpress/ /usr/share/nginx/html/
mkdir /usr/share/nginx/html/wp-content/uploads
chown -R admin:nginx /usr/share/nginx/html/*


#--------------------------------------
#network
#---------------------------------------

systemctl disable NetworkManager
systemctl stop NetworkManager

echo "DEVICE=enp0s3" > /etc/sysconfig/network-scripts/ifcfg-enp0s3
echo "BOOTPROTO=static" >> /etc/sysconfig/network-scripts/ifcfg-enp0s3
echo "IPADDR=192.168.254.10" >> /etc/sysconfig/network-scripts/ifcfg-enp0s3
echo "PREFIX=24" >> /etc/sysconfig/network-scripts/ifcfg-enp0s3
echo "GATEWAY=192.168.254.1" >> /etc/sysconfig/network-scripts/ifcfg-enp0s3
echo "DNS1=142.232.221.253" >> /etc/sysconfig/network-scripts/ifcfg-enp0s3

hostnamectl set-hostname wp21.lab471.htpbcit.ca
systemctl enable network
systemctl disable firstboot
shutdown -r now

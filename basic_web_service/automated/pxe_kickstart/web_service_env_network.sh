#!/usr/bin/env bash

#network settings
network_name=nasp_cm_co
network_address=192.168.254.0
cidr_bits=24


firewall-cmd --zone=public --add-port=50022/tcp --permanent
firewall-cmd --zone=public --add-port=50080/tcp --permanent
firewall-cmd --zone=public --add-port=50443/tcp --permanent

vboxmanage natnetwork remove --netname $network_name
vboxmanage natnetwork add --netname $network_name --network "$network_address/$cidr_bits" --dhcp off
#vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name:$protocol:[$global_ip]:$global_port:[$local_ip]:$local_port"

vboxmanage natnetwork modify --netname $network_name --port-forward-4 "rule1:tcp:[127.0.0.1]:50022:[192.168.254.5]:22"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "rule2:tcp:[127.0.0.1]:58080:[192.168.254.5]:80"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "rule3:tcp:[127.0.0.1]:50080:[192.168.254.10]:80"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "rule4:tcp:[127.0.0.1]:50443:[192.168.254.10]:443"

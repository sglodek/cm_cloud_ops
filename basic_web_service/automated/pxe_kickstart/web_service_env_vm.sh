#!/usr/bin/env bash

#copy files to pxe server
scp -P 50022 wp_ks.cfg admin@localhost:/usr/share/nginx/html
scp -P 50022 web_service_host_setup.sh admin@localhost:/usr/share/nginx/html
scp -P 50022 firstboot.service admin@localhost:/usr/share/nginx/html

#vm settings
vm_name='vm_wp'
#$vms_folder
network_name=nasp_cm_co

vboxmanage createvm --name $vm_name --register

vboxmanage storagectl $vm_name --name controller --add sata --bootable on --portcount 3

vboxmanage createmedium disk --filename ./$vm_name.vdi --size 8000
vboxmanage storageattach $vm_name --port 1 --storagectl controller --type hdd --medium ./$vm_name.vdi

vboxmanage storageattach $vm_name --storagectl controller --port 3 --type dvddrive --medium "/usr/share/virtualbox/VBoxGuestAdditions.iso"

vboxmanage modifyvm $vm_name\
    --ostype "RedHat_64"\
    --cpus 1\
    --hwvirtex on\
    --nestedpaging on\
    --largepages on\
    --firmware bios\
    --nic1 natnetwork\
    --nictype1 "82543gC"\
    --nat-network1 "$network_name"\
    --cableconnected1 on\
    --audio none\
    --boot1 disk\
    --boot2 net\
    --boot3 none\
    --boot4 none\
    --memory "2000"
